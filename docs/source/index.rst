Welcome to CommonOcean's documentation!
===================================

**CommonOcean** is a collection of composable benchmarks for motion planning of autonomous vessels and provides researchers with means of evaluating and comparing their motion planners. A benchmark consists of a scenario with a planning problem, a vessel model including vessel parameters, and a cost function composing a unique id. Along with benchmarks, we provide tools for motion planning. For further information and tutorials, check `our site <https://commonocean.cps.cit.tum.de/>`_. 

.. note::

   This project is under active development.
   
Documentation
--------

The full documentation of the API and introducing examples can be found under `commonocean.cps.cit.tum.de <https://commonocean.cps.cit.tum.de/>`_.

For getting started, we recommend our `tutorials <https://commonocean.cps.cit.tum.de/getting-started/>`_.

Contents
--------

.. toctree::
   :maxdepth: 2

   commonocean-io/commonocean/doc/docs/source/index
   commonocean-dc/doc/docs/source/index
